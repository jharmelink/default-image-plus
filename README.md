# README #

This is an extension of the [atlassian/default-image:2](https://hub.docker.com/layers/atlassian/default-image/2/images/sha256-897afb2491df8bc6fd73006341d70c4835167a812e534e65bfa40a9072037f3d?context=explore) image for building inside a Bitbucket pipleline.

### Additional packages ###

* awscli
* chromium
