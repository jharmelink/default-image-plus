FROM blueimp/chromedriver:latest

USER root

# Install base dependencies
RUN DEBIAN_FRONTEND="noninteractive" \
    && apt-get update \
    && apt-get install --no-install-recommends --no-install-suggests -y \
      less \
      groff \
      wget \
      git \
      ssh-client \
      zip \
      unzip \
      iputils-ping \
      awscli \
    && rm /bin/sh \
    && ln -s /bin/bash /bin/sh \
    # Remove obsolete files:
    && apt-get clean \
    && rm -rf \
      /tmp/* \
      /usr/share/doc/* \
      /var/cache/* \
      /var/lib/apt/lists/* \
      /var/tmp/*

# Create dirs and users
RUN mkdir -p /opt/atlassian/bitbucketci/agent/build \
    && userdel -r webdriver \
    && useradd --create-home --shell /bin/bash --uid 1000 pipelines \
    && sed -i '/[ -z \"PS1\" ] && return/a\\ncase $- in\n*i*) ;;\n*) return;;\nesac' /home/pipelines/.bashrc

USER pipelines

# Install nvm with node and npm
ENV NODE_VERSION=12.13.0 \
    NVM_DIR=/home/pipelines/.nvm \
    NVM_VERSION=0.33.8

RUN curl https://raw.githubusercontent.com/creationix/nvm/v$NVM_VERSION/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# Set node path
ENV NODE_PATH=$NVM_DIR/v$NODE_VERSION/lib/node_modules

# Install sdkman with multiple JDKs
ENV SDKMAN_DIR=/home/pipelines/.sdkman

RUN curl -s "https://get.sdkman.io" | bash && \
    chmod a+x "$SDKMAN_DIR/bin/sdkman-init.sh" && \
    source "$SDKMAN_DIR/bin/sdkman-init.sh" && \
    sdk install java 8.0.242.hs-adpt && \
    sdk install java 11.0.6.hs-adpt && \
    sdk install java 13.0.2.hs-adpt && \
    sdk install maven && \
    sdk default java 8.0.242.hs-adpt

# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8 \
    LC_ALL=C.UTF-8 \
    LANGUAGE=C.UTF-8

# Xvfb provide an in-memory X-session for tests that require a GUI
ENV DISPLAY=:99

# Set the path.
ENV PATH=$NVM_DIR:$NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

WORKDIR /opt/atlassian/bitbucketci/agent/build
ENTRYPOINT /bin/bash
